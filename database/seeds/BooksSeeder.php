<?php

use Illuminate\Database\Seeder;
use App\Author;
use App\Book;

class BooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Sample penulis
		$author1 = Author::create(['name'=>'Ika Natassa']);
		$author2 = Author::create(['name'=>'Fiersa Besari']);
		$author3 = Author::create(['name'=>'Tereliye']);

		// Sample buku
		$book1 = Book::create(['title'=>'Twivortiare', 'amount'=>3, 'author_id'=>$author1->id]);
		$book2 = Book::create(['title'=>'Garis Waktu', 'amount'=>2, 'author_id'=>$author2->id]);
		$book3 = Book::create(['title'=>'Bumi', 'amount'=>4, 'author_id'=>$author3->id]);
		$book4 = Book::create(['title'=>'Bulan', 'amount'=>3, 'author_id'=>$author3->id]);
    }
}
