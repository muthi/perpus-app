
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// const Foo = { template: '<div>foo</div>' }

import Index from './components/Index.vue';
import Create from './components/Create.vue';
import Author_index from './components/authors/Index.vue';
import Author_create from './components/authors/Create.vue';
import Author_update from './components/authors/Update.vue';

Vue.component('example', require('./components/Example.vue'));

const routes = [
  // { path: '/foo', component: Foo }
  { path: '/', component: Index },
  { path: '/home_vue', component: Author_index },
  { path: '/create', component: Create },
  { path: '/author_index', component: Author_index },
  { path: '/author_create', component: Author_create },
  { path: '/:id/edit', component: Author_update, name:'editAuthor' }
]

const router = new VueRouter({
  routes // short for `routes: routes`
})

const app = new Vue({
  router
}).$mount('#app-vue')
