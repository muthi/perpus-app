<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'AuthorsController@index');
Route::get('/home_vue', 'HomeController@index_vue')->name('home');

Route::group(['prefix'=>'admin', 'middleware'=>['auth', 'role:admin']], function () {
	// Route::resource('authors', 'AuthorsController');
	Route::resource('books', 'BooksController');
});

Route::resource('/authors', 'AuthorsController');
